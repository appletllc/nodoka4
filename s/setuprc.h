//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by setup.rc
//
#define IDS_nodokaSetup                 1
#define IDS_nodokaRunning               2
#define IDS_nodokaEmpty                 3
#define IDS_selectDir                   4
#define IDS_invalidDirectory            5
#define IDS_removeOk                    6
#define IDS_removeFinish                7
#define IDS_copyFinish                  8
#define IDS_error                       9
#define IDS_alreadyUninstalled          10
#define IDS_notAdministrator            11
#define IDS_invalidOS                   12
#define IDS_nodokaFile                  13
#define IDS_nodokaShellOpen             14
#define IDS_nodoka                      15
#define IDS_nodokad                     16
#define IDS_shortcutName                17
#define IDS_shortcutName2               18
#define IDS_keyboard109usb              19
#define IDS_keyboard104usb              20
#define IDS_dotnodoka                   21
#define IDS_copyFinishUsb               22
#define IDS_failedToReboot              23
#define IDS_installerror                24
#define IDS_detectfilterdriver          25
#define IDS_selectDirNODOKA             26
#define IDS_shortcutNameGUI             27
#define IDS_note01                      30
#define IDS_note02                      31
#define IDS_note03                      32
#define IDS_note04                      33
#define IDS_note05                      34
#define IDS_note06                      35
#define IDS_note07                      36
#define IDS_note08                      37
#define IDS_note09                      38
#define IDS_note10                      39
#define IDS_note11                      40
#define IDS_note12                      41
#define IDS_note13                      42
#define IDS_note14                      43
#define IDS_note15                      44
#define IDS_note16                      45
#define IDS_note17                      46
#define IDS_note18                      47
#define IDS_note19                      48
#define IDS_note20                      49
#define IDS_note21                      50
#define IDS_note22                      51
#define IDS_note23                      52
#define IDD_DIALOG_main                 101
#define IDI_ICON_nodoka                 102
#define IDC_EDIT_path                   1000
#define IDC_BUTTON_browse               1001
#define IDC_CHECK_registerStartMenu     1002
#define IDC_CHECK_registerStartUp       1003
#define IDC_COMBO_keyboard              1004
#define IDC_EDIT_note                   1005
#define IDC_CHECK_registerStartUp2      1006
#define IDC_CHECK_mouse                 1007
#define IDC_CHECK_keyboard              1008
#define IDC_CHECK_devicedriver          1009
#define IDC_CHECK_win8wa                1009
#define IDC_CHECK_keyboard2             1009
#define IDC_CHECK_limit                 1010
#define IDC_CHECK_registerStartUp3      1011
#define IDC_CHECK_HIL                   1012
#define IDC_EDIT_pathNODOKA             1013
#define IDC_BUTTON_browseNODOKA         1014
#define IDC_CHECKoffShortCut            1015
#define IDC_CHECKoffDotNodoka           1016
#define IDC_CHECK_ScancodeMapReload     1017
#define IDC_CHECK_dont_devicedriver     1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
