[Version]
Class=IEXPRESS
SEDVersion=3

[Options]
PackagePurpose=InstallApp
ShowInstallProgramWindow=0
HideExtractAnimation=0
UseLongFileName=1
InsideCompressed=0
CAB_FixedSize=0
CAB_ResvCodeSigning=0
RebootMode=N
InstallPrompt=
DisplayLicense=
FinishMessage=
TargetName=nodoka-4.30_sample_setup.exe
FriendlyName=Nodoka 4.30_sample
AppLaunched=setup.exe
PostInstallCmd=<None>
AdminQuietInstCmd=
UserQuietInstCmd=
SourceFiles=SourceFiles

[SourceFiles]
SourceFiles0=.\

[SourceFiles0]
..\Release\GuiEdit.exe=
..\Release\dotnet_starter.exe=
..\Release\nodoka.dll=
..\Release\nodoka_helper.exe=
..\Release\setup.exe=
..\Sample\nodoka.exe=
..\Sample\nodoka_hil.exe=
..\Sample\nodoka_limit.exe=
..\contrib\109onAX.nodoka=
..\contrib\98x1.nodoka=
..\contrib\DVORAKon109.nodoka=
..\contrib\ax.nodoka=
..\contrib\cursor.nodoka=
..\contrib\dvorak.nodoka=
..\contrib\dvorak109.nodoka=
..\contrib\ime.nodoka=
..\contrib\keitai.nodoka=
..\contrib\no_badusb.nodoka=
..\contrib\nodoka-settings.txt=
..\contrib\other.nodoka=
..\contrib\sample.nodoka=
..\doc\104.gif=
..\doc\109.gif=
..\doc\CONTENTS-en.html=
..\doc\CONTENTS-ja.html=
..\doc\CUSTOMIZE-en.html=
..\doc\CUSTOMIZE-ja.html=
..\doc\GUIEdit-ja.html=
..\doc\GuiEdit.png=
..\doc\MANUAL-en.html=
..\doc\MANUAL-ja.html=
..\doc\README-en.html=
..\doc\README-ja.html=
..\doc\README.css=
..\doc\banner-ja.gif=
..\doc\copy-contrib.png=
..\doc\copy-ja.png=
..\doc\edit-setting-ja.png=
..\doc\gui-edit-command-main-edited.png=
..\doc\gui-edit-command-wizard-1.png=
..\doc\gui-edit-command-wizard-2.png=
..\doc\gui-edit-command-wizard-3.png=
..\doc\gui-edit-command-wizard-include1.png=
..\doc\gui-edit-command-wizard-include2.png=
..\doc\gui-edit-command-wizard-keymap1.png=
..\doc\gui-edit-command-wizard-keymap2.png=
..\doc\gui-edit-command-wizard-keymap3.png=
..\doc\gui-edit-command-wizard-mod1.png=
..\doc\gui-edit-command-wizard-mod2.png=
..\doc\gui-edit-command-wizard-mod3.png=
..\doc\gui-edit-command-wizard-other1.png=
..\doc\gui-edit-command-wizard-other2.png=
..\doc\gui-edit-command-wizard-other3.png=
..\doc\gui-edit-cursor.nodoka.png=
..\doc\gui-edit-dot.nodoka.png=
..\doc\gui-edit-main-describe.png=
..\doc\gui-edit-main-loaded.png=
..\doc\gui-edit-right-click.png=
..\doc\gui-edit-sample.nodoka.png=
..\doc\gui-edit-setting1.png=
..\doc\gui-edit-setting2.png=
..\doc\gui-edit-start-new.png=
..\doc\icon0.png=
..\doc\investigate-ja.png=
..\doc\log-ja.jpg=
..\doc\menu-ja.png=
..\doc\pause-ja.png=
..\doc\regedit.png=
..\doc\setting-ja.png=
..\doc\setup0.jpg=
..\doc\setup1.jpg=
..\doc\setup3.jpg=
..\doc\syntax.txt=
..\doc\target.png=
..\doc\tasktray-icon.png=
..\doc\tasktray-icon7.png=
..\doc\tasktray-icon7help.png=
..\doc\tasktray-icon7help2.png=
..\doc\tasktray-icon7help3.png=
..\doc\version.jpg=
..\doc\version86.jpg=
..\doc\virtualstore-ja.png=
..\dot.nodoka\104.nodoka=
..\dot.nodoka\104on109.nodoka=
..\dot.nodoka\109.nodoka=
..\dot.nodoka\109on104.nodoka=
..\dot.nodoka\Common_Public_License_1_0.txt=
..\dot.nodoka\Common_Public_License_1_0_JP.txt=
..\dot.nodoka\Shift-F2_toggle_US-JP-Keyboard.nodoka=
..\dot.nodoka\add-mouse-gamepad.nodoka=
..\dot.nodoka\default.nodoka=
..\dot.nodoka\default2.nodoka=
..\dot.nodoka\dot.nodoka=
..\dot.nodoka\doten.nodoka=
..\dot.nodoka\dotjp.nodoka=
..\dot.nodoka\emacsedit.nodoka=
..\dot.nodoka\nodoka-mode.el=
..\dot.nodoka\nshell.exe=
..\dot.nodoka\nshell.txt=
..\dot.nodoka\nshell64.exe=
..\dot.nodoka\read-keyboard-define.nodoka=
..\dot.nodoka\readme-en.txt=
..\dot.nodoka\readme.txt=
..\gamepad\gamepad-mouse.nodoka=
..\gamepad\gamepad.dll=
..\gamepad\gamepad.nodoka=
..\gamepad\gamepad2-mouse.nodoka=
..\gamepad\gamepad64.dll=
..\sirius_sdk\sirius_hook_for_nodoka_x64.dll=
..\sirius_sdk\sirius_hook_for_nodoka_x86.dll=
..\ts4nodoka\ats4nodoka.dll=
..\ts4nodoka\ats4nodoka64.dll=
..\ts4nodoka\cts4nodoka.dll=
..\ts4nodoka\sts4nodoka.dll=
..\ts4nodoka\sts4nodoka64.dll=
..\ts4nodoka\thumbsense.nodoka=
..\x64\Release\nodoka64.dll=
..\x64\Release\setup64.exe=
..\x64\Sample\nodoka64.exe=
..\x64\Sample\nodoka64_hil.exe=
..\x64\Sample\nodoka64_limit.exe=
==
==
==
==
==
==
==
==
