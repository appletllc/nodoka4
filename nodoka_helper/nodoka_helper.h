#pragma once

#include "resource.h"

#ifndef WM_INPUT_DEVICE_CHANGE
#define WM_INPUT_DEVICE_CHANGE 0x00FE
#endif

#ifndef WM_INPUT
#define WM_INPUT 0x00FF
#endif
